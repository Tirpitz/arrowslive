from bootstrap3_datetime.widgets import DateTimePicker
from django.forms import ModelForm, DateField

from LiveScore.models import Archer, Club


class ArcherForm(ModelForm):
    class Meta:
        model = Archer
        exclude = ["id"]

    dob = DateField(
        widget=DateTimePicker(options={"format": "YYYY-MM-DD",
                                       "pickTime": False}))


class ClubForm(ModelForm):
    class Meta:
        model = Club
        exclude = ["id", "startDate"]


class AddOrUpdateForm(ModelForm):
    class Meta:
        model = Archer
        fields = "__all__"
