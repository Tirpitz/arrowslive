from django.apps import AppConfig


class LiveScoreConfig(AppConfig):
    name = 'LiveScore'
