# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 11:22
from __future__ import unicode_literals

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import LiveScore.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Archer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('dob', models.DateField()),
                ('handicap', models.IntegerField()),
                ('imageField', models.ImageField(blank=True, null=True, upload_to=LiveScore.models.get_image_path)),
                ('admin', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Arrow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
                ('comment', models.CharField(max_length=256)),
                ('length', models.FloatField()),
                ('spine', models.FloatField()),
                ('diameter', models.FloatField()),
                ('nock', models.CharField(max_length=256)),
                ('vanes', models.CharField(max_length=256)),
                ('tipWeight', models.FloatField()),
                ('weight', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='ArrowModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Bow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('brand', models.CharField(max_length=256)),
                ('length', models.FloatField()),
                ('braceHeight', models.FloatField()),
                ('tiller', models.CharField(max_length=256)),
                ('drawWeight', models.FloatField()),
                ('stabilizer', models.CharField(max_length=256)),
                ('clicker', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=256)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Archer')),
            ],
        ),
        migrations.CreateModel(
            name='BowClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='BowClassHandicap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('value', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='BowModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modelName', models.CharField(blank=True, max_length=256, verbose_name='Model Name')),
                ('length', models.FloatField(blank=True, verbose_name='Length')),
                ('braceHeight', models.FloatField(blank=True, verbose_name='Brace Height')),
            ],
        ),
        migrations.CreateModel(
            name='BowType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('handicap',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.BowClassHandicap')),
            ],
        ),
        migrations.CreateModel(
            name='CenterMark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Club',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('startDate', models.DateTimeField()),
                ('adminGroup',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='admin_group',
                                   to='auth.Group')),
                ('admins', models.ManyToManyField(to='LiveScore.Archer')),
                ('membersGroup',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='member_group',
                                   to='auth.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Coordinate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('x', models.FloatField()),
                ('y', models.FloatField()),
                ('z', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Dimension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('x', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='End',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField()),
                ('index', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('startTime', models.DateTimeField()),
                ('endTime', models.DateTimeField()),
                ('key', models.CharField(default='3E552D', max_length=16)),
                ('host', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Club')),
            ],
        ),
        migrations.CreateModel(
            name='EventEnd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(default='EDC74C', max_length=16)),
                ('index', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='EventRound',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('startTime', models.DateTimeField()),
                ('endTime', models.DateTimeField()),
                ('ended', models.BooleanField()),
                ('key', models.CharField(default='BF0890', max_length=16)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Event')),
                ('participants', models.ManyToManyField(to='LiveScore.Archer')),
            ],
        ),
        migrations.CreateModel(
            name='Gender',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Manufacturer Name')),
            ],
        ),
        migrations.CreateModel(
            name='Nock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Nock Name')),
                ('material', models.CharField(max_length=256, verbose_name='Material')),
                ('manufacturer',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer',
                                   verbose_name='Manufacturer Name')),
            ],
        ),
        migrations.CreateModel(
            name='Numbers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('material', models.CharField(max_length=256)),
                ('weight', models.FloatField()),
                ('shape', models.CharField(max_length=256)),
                ('manufacturer',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer')),
            ],
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('startTime', models.DateTimeField()),
                ('endTime', models.DateTimeField()),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Archer')),
            ],
        ),
        migrations.CreateModel(
            name='RoundTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='ScoringStyle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('showAsX', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Shaft',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('material', models.CharField(max_length=256)),
                ('weight', models.FloatField()),
                ('shape', models.CharField(max_length=256)),
                ('manufacturer',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer')),
            ],
        ),
        migrations.CreateModel(
            name='Shot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('x', models.FloatField()),
                ('y', models.FloatField()),
                ('score', models.IntegerField()),
                ('end', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.End')),
            ],
        ),
        migrations.CreateModel(
            name='SightSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('range', models.FloatField()),
                ('settingX', models.FloatField()),
                ('settingY', models.FloatField()),
                ('text', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('scoringStyle', models.IntegerField()),
                ('dimension', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='TargetDecoration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='TargetDrawable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='TargetModelBase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nameRes', models.IntegerField()),
                ('isFieldTarget', models.BooleanField()),
                ('faceRadius', models.IntegerField()),
                ('is3D', models.BooleanField()),
                ('centerMark',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.CenterMark')),
                ('decoration',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.TargetDecoration')),
                ('diameters', models.ManyToManyField(to='LiveScore.Dimension')),
                ('facePositions',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Coordinate')),
                ('scoringStyle',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.ScoringStyle')),
            ],
        ),
        migrations.CreateModel(
            name='Training',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('startTime', models.DateTimeField()),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Vane',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Vane Name')),
                ('length', models.FloatField(verbose_name='Length')),
                ('material', models.CharField(max_length=256, verbose_name='Material')),
                ('profile', models.CharField(max_length=256, verbose_name='Profile Shape')),
                ('manufacturer',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer')),
            ],
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='ZoneType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AddField(
            model_name='zone',
            name='zoneType',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.ZoneType'),
        ),
        migrations.AddField(
            model_name='targetmodelbase',
            name='zones',
            field=models.ManyToManyField(to='LiveScore.Zone'),
        ),
        migrations.AddField(
            model_name='target',
            name='drawable',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.TargetDrawable'),
        ),
        migrations.AddField(
            model_name='target',
            name='model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.TargetModelBase'),
        ),
        migrations.AddField(
            model_name='round',
            name='training',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Training'),
        ),
        migrations.AddField(
            model_name='round',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.RoundTemplate'),
        ),
        migrations.AddField(
            model_name='eventend',
            name='round',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.EventRound'),
        ),
        migrations.AddField(
            model_name='end',
            name='round',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Round'),
        ),
        migrations.AddField(
            model_name='bowmodel',
            name='manufacturer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer',
                                    verbose_name='Manufacturer'),
        ),
        migrations.AddField(
            model_name='bowclass',
            name='bowHandicap',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.BowClassHandicap'),
        ),
        migrations.AddField(
            model_name='bow',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.BowType'),
        ),
        migrations.AddField(
            model_name='arrowmodel',
            name='manufacturer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Manufacturer'),
        ),
        migrations.AddField(
            model_name='arrowmodel',
            name='nock',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Nock'),
        ),
        migrations.AddField(
            model_name='arrowmodel',
            name='point',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Point'),
        ),
        migrations.AddField(
            model_name='arrowmodel',
            name='shaft',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Shaft'),
        ),
        migrations.AddField(
            model_name='arrowmodel',
            name='vanes',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Vane'),
        ),
        migrations.AddField(
            model_name='arrow',
            name='numbers',
            field=models.ManyToManyField(to='LiveScore.Numbers'),
        ),
        migrations.AddField(
            model_name='arrow',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.Archer'),
        ),
        migrations.AddField(
            model_name='archer',
            name='bowClass',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LiveScore.BowClass'),
        ),
        migrations.AddField(
            model_name='archer',
            name='gender',
            field=models.ManyToManyField(to='LiveScore.Gender'),
        ),
    ]
