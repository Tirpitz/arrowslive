# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 11:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='arrow',
            name='name',
            field=models.CharField(default='My Arrow', max_length=200),
        ),
        migrations.AddField(
            model_name='eventround',
            name='index',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='775C54', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='7154C9', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='8425F8', max_length=16),
        ),
    ]
