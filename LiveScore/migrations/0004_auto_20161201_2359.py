# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-01 23:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0003_auto_20161201_0243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='3C1F99', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='32B9C8', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='73545A', max_length=16),
        ),
    ]
