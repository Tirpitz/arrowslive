# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-06 16:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0005_auto_20161206_0934'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='club',
            name='adminGroup',
        ),
        migrations.RemoveField(
            model_name='club',
            name='membersGroup',
        ),
        migrations.AddField(
            model_name='club',
            name='members',
            field=models.ManyToManyField(related_name='members', to='LiveScore.Archer'),
        ),
        migrations.AlterField(
            model_name='club',
            name='admins',
            field=models.ManyToManyField(related_name='admins', to='LiveScore.Archer'),
        ),
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='05A3B0', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='F2EC4D', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='E18CAD', max_length=16),
        ),
    ]
