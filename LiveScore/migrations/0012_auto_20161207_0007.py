# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-07 00:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0011_auto_20161206_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='club',
            name='admins',
            field=models.ManyToManyField(related_name='admins', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='club',
            name='members',
            field=models.ManyToManyField(related_name='members', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='437C5C', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='36D577', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='5FFBD1', max_length=16),
        ),
    ]
