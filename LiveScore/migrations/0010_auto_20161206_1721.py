# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-06 17:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0009_auto_20161206_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='club',
            name='passkey',
            field=models.CharField(default='join', max_length=50),
        ),
        migrations.AlterField(
            model_name='club',
            name='startDate',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='5BF113', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='1FDCD1', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='6C9701', max_length=16),
        ),
    ]
