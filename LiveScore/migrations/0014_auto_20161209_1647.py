# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-09 16:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('LiveScore', '0013_auto_20161208_0245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='key',
            field=models.CharField(default='52F1E1', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventend',
            name='key',
            field=models.CharField(default='6D1D42', max_length=16),
        ),
        migrations.AlterField(
            model_name='eventround',
            name='key',
            field=models.CharField(default='C8D791', max_length=16),
        ),
    ]
