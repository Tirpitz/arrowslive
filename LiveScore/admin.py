# Register your models here.
from django.contrib import admin

import LiveScore.models


class ArcherAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Archer, ArcherAdmin)


class EndAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.End, EndAdmin)


class ArrowAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Arrow, ArrowAdmin)


class BowClassAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.BowClass, BowClassAdmin)


class GenderAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Gender, GenderAdmin)


class EventAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Event, EndAdmin)


class numbersAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Numbers, numbersAdmin)


class ClubAdmin(admin.ModelAdmin):
    pass


admin.site.register(LiveScore.models.Club, ClubAdmin)
