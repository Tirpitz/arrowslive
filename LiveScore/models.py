import datetime
import os
import uuid

from django import forms
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, User, Group
from django.db import models
from django.db.models import CharField, ForeignKey, DateTimeField, BooleanField, ManyToManyField, CASCADE, \
    FloatField, IntegerField, ImageField
from django.template.defaultfilters import slugify
from django.urls import reverse


class PasswordField(forms.CharField):
    widget = forms.PasswordInput


class PasswordModelField(models.CharField):
    def formfield(self, **kwargs):
        defaults = {'form_class': PasswordField}
        defaults.update(kwargs)
        return super(PasswordModelField, self).formfield(**defaults)


class Manufacturer(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=256, verbose_name="Manufacturer Name")


class BowModel(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=models.CASCADE, verbose_name="Manufacturer")
    modelName = CharField(blank=True, max_length=256, verbose_name="Model Name")
    length = FloatField(blank=True, verbose_name="Length")
    braceHeight = FloatField(blank=True, verbose_name="Brace Height")


class Vane(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=CASCADE)
    name = CharField(max_length=256, verbose_name="Vane Name")
    length = FloatField(verbose_name="Length")
    material = CharField(max_length=256, verbose_name="Material")
    profile = CharField(max_length=256, verbose_name="Profile Shape")


class Nock(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=CASCADE, verbose_name="Manufacturer Name")
    name = CharField(max_length=256, verbose_name="Nock Name")
    material = CharField(max_length=256, verbose_name="Material")


class Point(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=CASCADE)
    name = CharField(max_length=256)
    material = CharField(max_length=256)
    weight = FloatField()
    shape = CharField(max_length=256)


class Shaft(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=CASCADE)
    name = CharField(max_length=256)
    material = CharField(max_length=256)
    weight = FloatField()
    shape = CharField(max_length=256)


class ArrowModel(models.Model):
    def __str__(self):
        return self.name

    manufacturer = ForeignKey(Manufacturer, on_delete=models.CASCADE)
    name = CharField(max_length=256)
    shaft = ForeignKey(Shaft)
    vanes = ForeignKey(Vane)
    nock = ForeignKey(Nock)
    point = ForeignKey(Point)


def get_image_path(instance, filename):
    return os.path.join('images', str(instance.id), filename)


class BowClass(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=256)
    bowHandicap = IntegerField()


class Gender(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=20)


class Archer(models.Model):
    name = CharField(max_length=200)
    bowClass = ForeignKey(BowClass)
    gender = ManyToManyField(Gender)
    dob = models.DateField()
    handicap = IntegerField()
    admin = ManyToManyField(User)
    imageField = ImageField(upload_to=get_image_path, blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def age(self):
        today = datetime.date.today()
        years_difference = today.year - self.dob.year
        is_before_birthday = (today.month, today.day) < (self.dob.month, self.dob.day)
        elapsed_years = years_difference - int(is_before_birthday)
        return elapsed_years


class Numbers(models.Model):
    def __str__(self):
        return str(self.number)

    number = IntegerField()


class Arrow(models.Model):
    def __str__(self):
        return self.name

    owner = ForeignKey(Archer, on_delete=CASCADE)
    number = IntegerField()
    comment = CharField(max_length=256)
    length = FloatField()
    spine = FloatField()
    diameter = FloatField()
    numbers = ManyToManyField(Numbers)
    nock = CharField(max_length=256)
    vanes = CharField(max_length=256)
    tipWeight = FloatField()
    weight = FloatField()
    name = CharField(max_length=200, default="My Arrow")


class SightSetting(models.Model):
    def __str__(self):
        return self.text

    range = FloatField()
    settingX = FloatField()
    settingY = FloatField()
    text = CharField(max_length=256)


class Bow(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=256)
    type = ForeignKey(BowClass)
    brand = CharField(max_length=256)
    length = FloatField()
    owner = ForeignKey(Archer, on_delete=CASCADE)
    braceHeight = FloatField()
    tiller = CharField(max_length=256)
    drawWeight = FloatField()
    stabilizer = CharField(max_length=256)
    clicker = CharField(max_length=256)
    description = CharField(max_length=256)


class RoundTemplate(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=256)


class ZoneType(models.Model):
    pass


class Zone(models.Model):
    zoneType = ForeignKey(ZoneType)
    pass


class Dimension(models.Model):
    x = FloatField()
    pass


class ScoringStyle(models.Model):
    missSymbol = "M"
    XSymbol = "X"
    showAsX = BooleanField()
    points = IntegerField
    pass


class Coordinate(models.Model):
    x = FloatField()
    y = FloatField()
    z = FloatField(default=0)
    pass


class CenterMark(models.Model):
    pass


class TargetDecoration(models.Model):
    pass


class TargetModelBase(models.Model):
    def __str__(self):
        return self.name

    nameRes = IntegerField()
    isFieldTarget = BooleanField()
    zones = ManyToManyField(Zone)
    diameters = ManyToManyField(Dimension)
    scoringStyle = ForeignKey(ScoringStyle)
    faceRadius = IntegerField()
    facePositions = ForeignKey(Coordinate)
    is3D = BooleanField()
    centerMark = ForeignKey(CenterMark)
    decoration = ForeignKey(TargetDecoration)


class TargetDrawable(models.Model):
    pass


class Target(models.Model):
    def __str__(self):
        return self.name

    scoringStyle = IntegerField()
    dimension = FloatField()
    model = ForeignKey(TargetModelBase)
    drawable = ForeignKey(TargetDrawable)


class Club(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=200, unique=True)
    admins = ManyToManyField(User, related_name="admins")
    # membersGroup = ForeignKey(Group, related_name="member_group")
    # adminGroup = ForeignKey(Group, related_name="admin_group")
    members = ManyToManyField(User, related_name="members")
    startDate = DateTimeField(auto_now_add=True)
    passkey = CharField(max_length=50, default="join")

    def get_absolute_url(self):
        path = reverse('clubs', args=[self.pk])
        return "http://%s%s" % (self.site, path)

    @property
    def slug(self):
        return slugify(self.name)


class Event(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=200)
    host = ForeignKey(Club, on_delete=models.CASCADE)
    startTime = DateTimeField()
    endTime = DateTimeField()
    key = CharField(default=uuid.uuid4().hex[:6].upper(), max_length=16)


class EventRound(models.Model):
    def __str__(self):
        return self.event + " round " + str(self.index)

    event = ForeignKey(Event, on_delete=CASCADE)
    participants = ManyToManyField(Archer)
    startTime = DateTimeField()
    endTime = DateTimeField()
    ended = BooleanField()
    index = IntegerField(default=1)
    key = CharField(default=uuid.uuid4().hex[:6].upper(), max_length=16)


class EventEnd(models.Model):
    def __str__(self):
        return self.event + " round " + str(self.round.index) + " end " + str(self.index)

    round = ForeignKey(EventRound)
    key = CharField(default=uuid.uuid4().hex[:6].upper(), max_length=16)
    index = IntegerField()


class Training(models.Model):
    def __str__(self):
        return self.name

    name = CharField(max_length=256)
    startTime = DateTimeField()
    event = ForeignKey(Event)


class Round(models.Model):
    type = ForeignKey(RoundTemplate, on_delete=CASCADE)
    owner = ForeignKey(Archer, on_delete=CASCADE)
    training = ForeignKey(Training, on_delete=CASCADE)
    startTime = DateTimeField()
    endTime = DateTimeField()


class End(models.Model):
    time = DateTimeField()
    round = ForeignKey(Round, on_delete=CASCADE)
    index = IntegerField()


class Shot(models.Model):
    end = ForeignKey(End, on_delete=CASCADE)
    x = FloatField()
    y = FloatField()
    score = IntegerField()
