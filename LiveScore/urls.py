from django.conf.urls import url, include

from . import views

urlpatterns = [
    # python-social-auth
    url(r'^user/(?P<pk>[-\w]+)/$', views.DetailViewUser.as_view(), name='user-detail'),
    url(r'^users', views.UserList.as_view(), name="list-clubs"),
    url(r'^email-sent/', views.validation_sent, name="email-sent"),
    url(r'^login/$', views.login, name="login"),
    url(r'^logout/$', views.Logout.as_view(), name="logout"),
    url(r'^done/$', views.Done.as_view(), name='done'),
    url(r'^ajax-auth/(?P<backend>[^/]+)/$', views.ajax_auth, name='ajax-auth'),
    # todo: finish email pipeline
    url(r'^email/$', views.require_email, name='require_email'),
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    url(r'^view', views.view_session, name='ScoreBoard'),
    url(r'^archercp', views.archerCP, name="Manage Archers"),

    # clubs
    url(r'^clubs', views.clubList.as_view(), name="View-Clubs"),
    url(r'^club/new', views.NewClub.as_view(), name="New Club"),
    url(r'^club/(?P<pk>[-\w]+)/$', views.DetailViewClub.as_view(), name='club-detail'),
    url(r'^club/(?P<pk>[-\w]+)/members$', views.ClubMembers.as_view(), name='club-members'),
    url(r'^club/(?P<pk>[-\w]+)/admins', views.ClubAdmins.as_view(), name='club-members'),

    # archers
    url(r'^archers', views.ArcherList.as_view(), name="View-Clubs"),
    url(r'^user/(?P<pk>[-\w]+)/archers$', views.UserArchers.as_view(), name='user-archers'),
    url(r'^archers/new/$', views.NewClub.as_view(), name="New Club"),
    url(r'^archer/(?P<pk>[-\w]+)/$', views.DetailViewArcher.as_view(), name='archer-detail'),

    # general
    url(r'^$', views.clubList.as_view(), name="home"),

]
