from django.contrib.auth.models import Permission
from django.http import request


def get_user_privileges(request: request):
    user = request.user
    if user.is_superuser:
        return set(Permission.objects.all())
    perms = set(user.user_permissions.all() | Permission.objects.filter(group__user=user))
    return perms
