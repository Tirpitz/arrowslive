import json

from django.conf import settings
from django.contrib.auth import logout as auth_logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils import timezone
from django.views import View
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from social.apps.django_app.utils import psa
from social.backends.google import GooglePlusAuth
from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.utils import load_backends

from LiveScore.decorators import render_to
from LiveScore.models import Club, Archer
from .forms import ArcherForm, ClubForm


def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/')


def context(**extra):
    return dict({
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
        'plus_scope': ' '.join(GooglePlusAuth.DEFAULT_SCOPE),
        'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS)
    }, **extra)


@render_to('home.html')
def login(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('done')
    return context()


class Done(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return redirect("/")


@render_to('home.html')
def validation_sent(request):
    return context(
        validation_sent=True,
        email=request.session.get('email_validation_address')
    )


class Logout(View):
    """Logs out user"""

    def get(self, request, **kwargs):
        auth_logout(self.request)

        return redirect('login')


@render_to('home.html')
def require_email(request):
    backend = request.session['partial_pipeline']['backend']
    return context(email_required=True, backend=backend)


@psa('social:complete')
def ajax_auth(request, backend):
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')


def view_session(request):
    return render(request, 'LiveScore\scoreBoard.html', context={"title": "Score Board"})


# todo:convert to CBV
@login_required
def archerCP(LoginRequiredMixin, request):
    archerForm = ArcherForm
    if request.method == 'POST':
        archerForm = archerForm(request.POST)

        if archerForm.is_valid():
            archerForm.save()

        return render(request=request, template_name="LiveScore/ArcherCP.html",
                      context={"archerForm": ArcherForm(request.POST)})

    return render(request=request, template_name="LiveScore/ArcherCP.html", context={"archerForm": archerForm()})


class clubList(ListView):
    model = Club

    def get_context_data(self, **kwargs):
        context = super(clubList, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class NewClub(PermissionRequiredMixin, CreateView):
    permission_required = "LiveScore.add_club"
    model = Club
    form_class = ClubForm
    template_name = "LiveScore/club_form.html"
    # todo: impliment add creator to admins and members on creation


class DetailViewClub(DetailView):
    model = Club
    template_name = "LiveScore/club_detail.html"
    title = "hello"

    def get(self, request, **kwargs):
        self.object = self.get_object()
        if request.user in self.object.admins.all():  # user is admin of this club
            # todo: club control panel
            self.title = "My Club control Panel"
            pass
        else:
            self.title = "Club Profile"
        return render(request=request, template_name=self.template_name, context=self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(DetailViewClub, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context["title"] = self.title
        return context


class ClubMembers(LoginRequiredMixin, ListView):
    model = User
    template_name = "LiveScore/members.html"
    title = "Club Members"

    def get_queryset(self):
        club = Club.objects.get(id=self.kwargs['pk'])
        qs2 = club.members.all()
        return qs2

    def get_context_data(self, **kwargs):
        context = super(ClubMembers, self).get_context_data(**kwargs)
        context["title"] = self.title
        return context


class DetailViewArcher(LoginRequiredMixin, DetailView):
    model = Archer

    def get_context_data(self, **kwargs):
        context = super(DetailViewArcher, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context["title"] = self.title
        return context


class DetailViewUser(LoginRequiredMixin, DetailView):
    model = User
    title = "hello"
    template_name = "auth/user_detail.html"

    def get(self, request, **kwargs):
        self.object = self.get_object()
        if self.kwargs["pk"] == str(request.user.pk):  # authenticated user is viewing self
            # todo: user  control panel
            self.title = "My control Panel"
            pass
        else:

            self.title = "User Profile"
        return render(request=request, template_name=self.template_name, context=self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super(DetailViewUser, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context["title"] = self.title
        return context


class ClubAdmins(LoginRequiredMixin, ListView):
    model = User
    template_name = "LiveScore/members.html"
    title = "Club Admins"

    def get_queryset(self):
        club = Club.objects.get(id=self.kwargs['pk'])
        qs2 = club.admins.all()
        return qs2

    def get_context_data(self, **kwargs):
        context = super(ClubAdmins, self).get_context_data(**kwargs)
        context["title"] = self.title
        return context


# todo: add invite member and make admin to member views
# todo: invite to club if not in userts send invitation (social auth integration)
# todo: menu middleware options
class UserArchers(LoginRequiredMixin, ListView):
    model = Archer
    template_name = "LiveScore/user_archers.html"
    title = "User's Archers"

    def get_queryset(self):
        archers = Archer.objects.all().filter(admin=User.objects.get(id=self.kwargs['pk']))

        return archers

    def get_context_data(self, **kwargs):
        context = super(UserArchers, self).get_context_data(**kwargs)
        context["title"] = self.title
        return context

    pass


class ArcherList(LoginRequiredMixin, ListView):
    model = Archer

    def get_context_data(self, **kwargs):
        context = super(ArcherList, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

    pass


class UserList(LoginRequiredMixin, ListView):
    model = User

    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

    pass
