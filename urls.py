from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^add/(?P<model_name>\w+)/?$', 'tekextensions.views.add_new_model'),
    url(r'^', include('LiveScore.urls'))
]
